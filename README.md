# 手眼标定论文及代码
- 包含C++版本完整工程
- 包含Python版本代码
- 包含Matlab版本代码

### C++工程使用方式
C++版本直接调用opencv库函数，所以对opencv有依赖。

```
cd cpp/handeye-cpp
mkdir build
cmake ..
make
```

### Python使用方式
```
pip install numpy
pip install transforms3d
python3 tsai.py
```



@Autor SangXin
@Csdn https://blog.csdn.net/qq_27865227/article/details/114266140
